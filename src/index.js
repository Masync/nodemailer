//  *** Libs
const express = require('express');
const path = require('path');
const app = express();
//  *** middlewares
app.use(express.urlencoded({extended: false}));
app.use(express.json());
app.use(require('./routes/index'));
app.use(express.static(path.join(__dirname, 'public')));
//  *** Server
app.listen(3000, () => {console.log('port 3000');});