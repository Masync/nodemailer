const {Router} = require('express');
const routes = Router();
const controller = require('../controllers/sendMail.controller')

routes.post('/send-email', controller.sendEmail);

module.exports = routes;