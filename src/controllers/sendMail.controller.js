const nodemailer = require('nodemailer');

const sendEmail = async (req, res) =>{
    const {name, email, phone, message} =  req.body;
    
    contentHTML = 
        `
        <h1> User info </h1>

        <ul>
            <li> Username: ${name}</li>
            <li> Email: ${email}</li>
            <li> phone: ${phone}</li>
        </ul>
        <p> Message: ${message}</p>
        `;

    const transporter = nodemailer.createTransport({
        host: 'mail.unisabaneta.xyz',
        port: 26,
        secure: false,
        auth: {
            user: 'soporte@unisabaneta.xyz',
            pass: 'unisabaneta123'
        },
        rejectUnauthorizes: false
    });

     await transporter.sendMail({
        from: "sender@server.com",
        to: "receiver@sender.com",
        subject: "Message title",
        text: "Plaintext version of the message",
        html: contentHTML
    });

    res.redirect('/success.html');
};

module.exports = {sendEmail}